# oak provenance dendro



## Introduction

In this repository I: 

- discovered increment core analyses (data_pro_discovery.Rmd)

- started exploring the data as well as an exemple dataset from the R package dplR (data_exploration.Rmd)

- modified the main scripts (see script folder) used to convert the raw datafiles provided by phenobois to R objects. 

- started analysing the oak increment core data (data_analysis.Rmd)

## Issues

- Many series are considered "dirty dogs" by the package, and I don't understand why. 

- series include many identical ring width values, and I'm not sure why. 

- I need to read more to know how to analyse this data... 

 

