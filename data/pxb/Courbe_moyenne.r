#source("Courbe_moyenne.r")

#Changer ici le titre :
titre<-"Abiogene 2021"

#Lecture du fichier essai.txt
don<-read.table("essai.txt", header=TRUE, sep=" ", na.strings="NA")

#MillÚsime le plus petit et le plus grand
minmill<-min(don$mil)
maxmill<-max(don$mil)

#Moyenne par millÚsime pour les rayons A :
for (i in minmill:maxmill)
    {
     tAtA<-don[c(grep(pattern="A$",don$code), grep(pattern="A.$",don$code)),]
     moyA<-mean(tAtA[tAtA$mil==i , 7])
     quant125A<-quantile(tAtA[tAtA$mil==i , 7], 0.125)
     quant875A<-quantile(tAtA[tAtA$mil==i , 7], 0.875)

     tBtB<-don[c(grep(pattern="B$",don$code), grep(pattern="B.$",don$code)),]
     moyB<-mean(tBtB[tBtB$mil==i , 7])
     quant125B<-quantile(tBtB[tBtB$mil==i , 7], 0.125)
     quant875B<-quantile(tBtB[tBtB$mil==i , 7], 0.875)

     moyTot<-mean(don[don$mil==i , 7])
     quant125Tot<-quantile(don[don$mil==i , 7], 0.125)
     quant875Tot<-quantile(don[don$mil==i , 7], 0.875)
     
     if (i == minmill) {result<-data.frame("Mil"=i, "moyA"=moyA, "quant125A"=quant125A, "quant875A"=quant875A,
                        "moyB"=moyB, "quant125B"=quant125B, "quant875B"=quant875B,
                        "moyTot"=moyTot, "quant125Tot"=quant125Tot, "quant875Tot"=quant875Tot)
                        } else {result<-rbind(result, c("Mil"=i, "moyA"=moyA, "quant125A"=quant125A, "quant875A"=quant875A,
                        "moyB"=moyB, "quant125B"=quant125B, "quant875B"=quant875B,
                        "moyTot"=moyTot, "quant125Tot"=quant125Tot, "quant875Tot"=quant875Tot)) }
    
    }
    
#Graphique

legend("bottom",lty=c("solid","dashed","dashed","solid"), lwd=c(2,1,1,1), col=c("black","red","blue","grey"),
legend=c("moyenne", "moyenne rayon A", "moyenne rayon B", "quantiles 75%"), bg="white")
