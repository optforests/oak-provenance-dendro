#source("Nettoyage_PXB.r")

#Choix du dernier cerne complet (DCC) pour le nettoyage :
dcc<-"2021"      #A modifier par l'utilisateur ICI

#Liste des fichiers .pxb dans le r�pertoire courant :
fichiers.pxb<-list.files(getwd(), all.files=FALSE,
full.names=FALSE, recursive=FALSE, pattern=".pxb$")

#Boucle sur les noms de fichier :

for (i in fichiers.pxb)
    {
  input<-file(i, open="r")     #Lecture du fichier
  content<-readLines(input)
  ligne<-grep(pattern=paste("\t",dcc,"\t",sep=""), content, value=FALSE)+3     #recherche de la ligne du DCC
  if (length(ligne) == 0)         #Si la ligne du DCC n'est pas trouv�e (arbre mort avant le DCC sp�cifi�)
      {
      writeLines(content, con = paste("Pas_Nettoye_",i, sep=""))
      } else {
          content<-content[-c(ligne:length(content))]
          writeLines(content, con = paste("Nettoye_",i, sep=""))
          close(input)
        }
    }
    
    