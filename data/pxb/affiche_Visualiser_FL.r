#source("affiche_Visualiser_FL.r");affiche_Visualiser_FL.r()

# affiche le profil enregistre dans l'environnement R (attention: modifier le "pm" si une autre extension a ete choisie)


affiche_Visualiser_FL.r<-function()
{
#faire defiler ttes les carottes avec une pause entre chaque
# attention: ajouter 'readline() � la fin du programme
liste<-objects(pattern=".pxb$",pos=1)                          

# regarder par carotte
#print("carotte a visualiser")
#code<-readline()
#liste<-paste("Nettoye_S",code,".pxb$",sep="")

print(liste)
for (i in 1:length(liste)) 
	{
	nom<-liste[i]
      print(i);print(nom)
	donnees<-get(nom,pos=1)
	profil<-donnees$profil
	limites<-donnees$limites
	mil<-donnees$millesimes
	x<-seq(along=profil)
	plot(x,profil)
	lines(x,profil,col=1,lwd=1)
        abline(v = limites,col=2,lwd=2)     #abline(0,1)
        points(limites, profil[limites], pch = 3, cex = 2,col=3,lwd=2)

	position<-(c(0,limites)-c(limites,NA))/2
	position<-position[-length(position)]
	par(srt=270)
	text(limites+position,max(profil)-0.01,as.character(mil))
	title(nom)
	
readline()   # commande pour regarder les carottes une par une

	}

}



