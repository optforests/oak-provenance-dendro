#source("calcul2_CL.r");calcul2.r()

#calcule densite min, max et moy ... par echantillon et par cerne
#et stocke dans un tableau nomme "essai"


calcul2.r<-function()
{
liste<-objects(pattern=".pxb$",pos=1)
print(length(liste))
readline()
mat<-NULL
code2<-NULL
for (i in  1:length(liste))
	{
	nom<-liste[i]	
	print(i)
	print(nom)
	donnees<-get(nom,pos=1)
	profil<-donnees$profil
	limites<-c(0,donnees$limites)
				#chaque cerne est, dans ce programme, associ? ? la limite inf?rieure (car on ajoute "0" au vecteur "limites")
	mil<-donnees$millesimes

	for (j in  1:(length(limites)-1))
		{
		cerne<-profil[(limites[j]+1):limites[j+1]]
		l<-length(cerne)*0.021167
		surf<-pi*((l*l)+(2*limites[j]*0.021167*l)) # surface du cerne
		m<-mean(cerne)
		mi<-min(cerne)
		ma<-max(cerne)
				#plot(cerne)
		milcerne<-mil[j]
				#abline(h=c((mi+ma)/2,mi,ma),col=c(3,6,6))
				#title(main=paste(nom,milcerne))
		mid<-(mi+ma)/2			
		ordcerne<-sort(cerne)
				#plot(cerne)
				#abline(h=c(mid,mi,ma),col=c(3,6,6))
				#readline()
				#plot(ordcerne)
				#abline(h=c(mid,mi,ma),col=c(3,6,6))
		BI<-ordcerne[ordcerne<mid]
#print(milcerne)
#print(ordcerne)
#print(mid)
#print(BI)
#print(var(BI))
#readline()
		BF<-ordcerne[ordcerne>mid]
				#plot(BI)
				#abline(h=c(mid,mi,ma),col=c(3,6,6))
				#plot(BF)
				#abline(h=c(mid,mi,ma),col=c(3,6,6))
		mBI<-mean(BI)
		mBF<-mean(BF)
		largBI<-length(seq(along=BI))
		largBF<-length(seq(along=BF))
		pBF<-largBF*100/(largBI+largBF)
		biom<-m*surf
			
			bio<-0
			for (k in 1:(length(cerne)))
			{
			r<-(limites[j]+k)*0.021167
			airepixel<-(pi)*(2*r*0.021167+0.021167*0.021167)
			biopixel<-airepixel*cerne[k]
			bio<-bio+biopixel
			}

		mpond<-bio/surf

		het<-ma-mi
			n<-length(ordcerne)
			n<-round(n*0.05) #on prend 5% des valeurs de densite intracerne
			min5<-mean(ordcerne[1 : n])
			max5<-mean(ordcerne[(length(ordcerne)-n) : length(ordcerne)])
			het5<-max5-min5
		#varintracerne<-var(cerne)
		#varintraBI<-var(BI)
		#varintraBF<-var(BF)

#		codeprofil<-as.numeric(substring(nom,2,5))
#		code2<-c(code2,substring(nom,2,5))
#		assign("codeprofil",code2,immediate=T,pos=1)

    codeprofil<-strsplit(liste[i],"_")[[1]][2]
    codeprofil<-strsplit(codeprofil,".pxb")[[1]][1]
		mat<-rbind(mat,c(j,codeprofil,j,milcerne,l,surf,m,mpond,mi,ma,biom,bio,mid,mBI,mBF,pBF,min5,max5,het,het5))
		dimnames(mat)<-list(NULL,list("n","code","ac","mil","l","surf","m","mpond","mi","ma","biom","bio","mid","mBI","mBF","pBF","min5","max5","het","het5"))
		assign("essai",mat,immediate=T,pos=1)
#readline()
		write.table(essai,file="essai.txt",row.names=FALSE)

		}
	}


}

