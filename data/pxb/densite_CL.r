#source("densite_CL.r");densite.r()

#rentre les donnees de densite sous l'environnement R

# possibilite de modifier le repertoire de travail avec la ligne de commande suivante
# par defaut c'est le repertoire ou se trouve l'envirronement R qui est utilise
# setwd("D:/Documents/DonneesR/Densite")

# Modifier le pattern = par la lettre de d�but des fichiers

densite.r<-function()
{
par(mfrow = c(1, 1), pty = "m")
liste <- list.files(path=".",pattern=".pxb$")
noms <- liste
reste <- liste
       for(i in 1:length(reste))
                {
                profil.sauve <- NULL
                x.sauve <- NULL
                limil.sauve <- NULL
                fichier <- reste[i]
                nom <- fichier
                print(nom)
                assign(nom, matrix(as.numeric(scan(fichier, skip = 9, multi.line = T)), byrow = T, ncol = 2), pos = 1, immediate = T)
        #recuperation de donnees Windendro.
                donnees <- get(nom, pos = 1)
                profil <- donnees[, 1]
                profil<-profil[profil<2]
                profil<-profil[profil>0]
                limil <- donnees[, 2]
                limites <- seq(along = donnees[, 2])[donnees[, 2] > 1]
                profil<-profil[1:max(limites)] 
	#eliminer ce qui est au dela de la derniere limite
                lp <- length(profil)    
	#longueur du profil
                x.profil<-seq(along=profil)
                par(srt=270)
                plot(x.profil,profil,cex=.4)
                lines(x.profil,profil,col=1)
                abline(v = limites,col=2,lwd=2)     #abline(0,1)
                points(limites, profil[limites], pch = 3, cex = 2,col=3,lwd=2)
position<-(c(0,limites)-c(limites,NA))/2
position<-position[-length(position)]
text(limites+position,max(profil)-0.01,as.character(limil[limil>1]))
title (nom)
	#sauvegarde
                sortie <- list(length = 3)
                sortie[[1]] <- profil[!is.na(profil)]
                sortie[[2]] <- seq(along = limil)[limil > 1]
                sortie[[2]] <- sortie[[2]][!is.na(sortie[[2]])]
                sortie[[3]] <- limil[limil>1]
                names(sortie) <- c("profil", "limites","millesimes")
                #readline()
                assign(paste(nom, sep = ""), sortie, pos = 1, immediate = T)
        }
}

