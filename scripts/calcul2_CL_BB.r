#source("calcul2_CL.r");calcul2.r()

#calcule densite min, max et moy ... par echantillon et par cerne
#et stocke dans un tableau nomme "essai"

##pixel in millimiters? 

calcul2.r<-function(res, pixel=0.021167){
  fill=1
  out=data.frame()
  pb = txtProgressBar(min = 0, max = length(res), initial = 0, style=3)
  for (i in  names(res)){
    if(is.na(res[[i]]$tree_diam)==F){
      prog=which(names(res)==i)
      limites<-c(0,res[[i]]$limites)
      #chaque cerne est, dans ce programme, associé la limite superieur (car on ajoute "0" au vecteur "limites") 
      ## in the previous version the sentence said "inferieur", which didn't make much sens to me
      for (j in length(limites):2){ ##pour chaque année
        cerne<-res[[i]]$profil[((limites[j-1])+1):limites[j]]
        w<-length(cerne)*pixel ##larger du cerne
        if(j==length(limites)){
          R=(res[[i]]$tree_diam/2)-res[[i]]$bark ##grand rayon du cerne le plus grand.
        }else{
          R=out[fill-1, "innerdiam"]-pixel ##res[[i]]$inside_diams[j+1]
        }
        r=R-w
        res[[i]]$inside_diams[j]=r
        out[fill, "core"]=i
        out[fill,"milcerne"]<-res[[i]]$millesimes[j-1] 
        out[fill, "outerdiam"]=R
        out[fill, "innerdiam"]=r
        out[fill, "width"]=w
        out[fill, "surf"]<-pi*((r^2)+(2*w*r)) # surface du cerne
        out[fill, "m"]<-mean(cerne)
        out[fill, "mi"]<-min(cerne)
        out[fill,"ma"]<-max(cerne)
        ##
        out[fill, "mid"]<-(out[fill, "mi"]+out[fill, "ma"])/2			
        ordcerne<-sort(cerne)
        BI<-ordcerne[ordcerne<out[fill, "mid"]]
        BF<-ordcerne[ordcerne>out[fill, "mid"]]
        out[fill,"mBI"]<-mean(BI)
        out[fill, "mBF"]<-mean(BF)
        out[fill, "largBI"]<-length(seq(along=BI))
        out[fill, "largBF"]<-length(seq(along=BF))
        out[fill, "pBF"]<-out[fill, "largBF"]*100/(out[fill, "largBI"]+out[fill, "largBF"])
        out[fill, "biom"]<-out[fill, "m"]*out[fill, "surf"]
        bio<-0
        for (k in 1:(length(res[[i]]$profil))){
          airepixel<-(pi)*(2*r*pixel+pixel^2) #r is still my inner diameter
          biopixel<-airepixel*res[[i]]$profil[k]
          bio<-bio+biopixel
        }
        out[fill, "mpond"] <- bio/out[fill, "surf"]
        out[fill, "het"] <- out[fill, "ma"]-out[fill, "mi"]
        n<-length(ordcerne)
        n<-round(n*0.05) #on prend 5% des valeurs de densite intracerne
        out[fill, "min5"] <-mean(ordcerne[1 : n])
        out[fill, "max5"] <-mean(ordcerne[(length(ordcerne)-n) : length(ordcerne)])
        out[fill, "het5"]<-out[fill, "max5"]-out[fill, "min5"]
        fill=fill+1
      }
      setTxtProgressBar(pb,prog)
    }
  }
  close(pb)
  return(out)
}


